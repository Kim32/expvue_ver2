class CreateFolders < ActiveRecord::Migration
  def change
    create_table :folders do |t|
      t.string :label
      t.string :ancestry

      t.timestamps null: false
    end
  end
end
